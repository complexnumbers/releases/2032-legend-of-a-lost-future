# Build files for "2032: Legend of a Lost Future" album

Index file: https://complexnumbers.gitlab.io/releases/2032-legend-of-a-lost-future/

## License
### 2032: Legend of a Lost Future (c) by Victor Argonov Project

2032: Legend of a Lost Future is licensed under a
Creative Commons Attribution-ShareAlike 4.0 International License.

You should have received a copy of the license along with this
work. If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
